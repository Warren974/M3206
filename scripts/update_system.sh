#! /bin/bash

if [ "$EUID" -ne 0 ];#$EUID est le nom de l'utilisateur actuel 
then
	echo "[/!\]vous devez etre super-utilisateur[/!\]"
else
	echo "update database"
	apt-get update 1>> /dev/null
	echo "upgrade system"
	apt-get upgrade 1>> /dev/null
fi
