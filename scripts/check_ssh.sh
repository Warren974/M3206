#! /bin/bash

if (dpkg -s ssh | grep install > "Status: install OK installed" );
then 
	echo "ssh est installé"
else
	echo "ssh n'est pas installé"
	apt-get install ssh
fi

if (service ssh status | active > "Active: active");
then 
	echo " SSH: le service est lancé "
else 
	echo "SSH: le service n'est pas lancé"
	echo "lancement du service"
	service ssh start >> /dev/null
fi
