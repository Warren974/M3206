#! bin/bash
path=$(readlink -f $1) 
echo $path > $path/path.txt
DATE=$(date +%Y_%m_%d_%H%M)
TARTARGET=/tmp/backup/$(date +%Y_%m_%d_%H%M)_$(basename $1).tar.gz 

echo "-----------------------";
echo " - sauvegarde ";
echo "-----------------------";
echo "";

echo "Creation de l'archive"
sudo tar cvzf $TARTARGET $1 #on cree l'archive 
echo "----------------------";
echo "";

echo "Verfi existence de l'archive";
if [ -e /tmp/backup/$(date +%Y_%m%d_%H%M)_$(basename $1).tar.gz ] #on verifie si l'archive a bien ete cree
then
echo ""
echo "Votre archive est crée";
echo ""
else
echo ""
echo "pas de creation";
echo ""
fi

echo "### Fin de la sauvegarde  ###";
