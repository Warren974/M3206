#!/bin/bash

echo "Checking internet connection"

wget -q --tries=10 --timeout=1000 --spider http://google.com #on defini le temps de la requete sur 1000s et on defini 10 essaieon utilise spider pour juste verifier et ne pas telecharger
if [[ $? -eq 0 ]]; then  #si on a une reponse 
	echo "[/!\]Internet is OK[/!\]"
else
	echo "[/!\] Not connected to Internet[/!\]"
	echo "[/!\] Please check configuration [/!\]"
fi
