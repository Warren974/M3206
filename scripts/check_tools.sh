#! /bin/bash

if (dpkg -s git | grep install > "Status: install OK installed" );
#la commande dpkg --status va nous donner le status entier du paquet choisi puis avec grep on va rechercher precisement si le paquet est installé
then
	echo "git: installé"
else	
	echo "git: pas installé"
	apt-get install git
fi

if (dpkg -s tmux | grep install > "Status: install OK installed ");
then
	echo "tmux: installé"
else	
	echo "tmux: pas installé"
	apt-get install tmux
fi

if (dpkg -s vim | grep install > "Status: install OK installed ");
then
	echo "vim: installé"
else	
	echo "vim: pas installé"
	apt-get install vim
fi

if (dpkg -s htop | grep install > "Status: install OK installed ");
then
	echo "htop: installé"
else	
	echo " htop: pas installé "
	apt-get install htop
fi

