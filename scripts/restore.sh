#!/bin/sh

echo "---------------";
echo "Restauration du repertoire";
echo "-----------------";
echo "";

echo "Recuperation de l'archive";

cd /home/Oneesama/M3206/restauration

date=$(date +%Y_%m_%d_%H%M) 
sudo mkdir $date
cd $date
tar -xvzf /tmp/backup/2016*
echo "----------------";
echo "";
 
echo "### Fin de l'extraction des fichiers. ###";
